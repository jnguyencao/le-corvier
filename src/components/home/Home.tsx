import React from "react"
import Featured from "./featuredServices/Featured"
import Hero from "./hero/Hero"
import Room from "./room/Room"

const Home = () => {
  return (
    <>
      <Hero />
      <Featured />
      <Room />
    </>
  )
}

export default Home

import { GB, FR, VN } from 'country-flag-icons/react/3x2'

interface Props {
  language: string
}

export default function LanguageIcon({ language }: Props) {
  switch (language) {
    case 'en':
      return <GB className="country-flag" />
    case 'fr':
      return <FR className="country-flag" />
    case 'vn':
      return <VN className="country-flag" />
  }

  return null
}
